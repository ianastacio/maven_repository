add the following to your project's pom file:

    <repositories>
        <repository>
            <id>ianastacio-maven-repository</id>
            <url>https://bitbucket.org/ianastacio/maven_repository/raw/master/repository</url>
            <snapshots>
                <enabled>true</enabled>
            </snapshots>
        </repository>
    </repositories>
