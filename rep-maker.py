#!/usr/bin/python

import sys
import os.path
import subprocess
import ntpath


if len(sys.argv) < 2:
    sys.stderr.write('Usage: full_path_to_jar version\n')
    sys.exit(1)

jar_path = sys.argv[1]
filename = ntpath.basename(jar_path)
ver = sys.argv[2]
group_id = 'info.ianastacio.utils'

f = open('dependencies.xml', 'w')
f_commands = open('mvn_commands', 'w')

print jar_path + '\n'
# create dependencies for pom.xml
f.writelines('<dependency>\n')
f.writelines('<groupId>' + group_id + '</groupId>\n')
f.writelines('<artifactId>' + filename[:-4] + '</artifactId>\n')
f.writelines('<version>' + ver + '</version>\n')
f.writelines('</dependency>\n\n')

# create dependencies for pom.xml
command = 'mvn install:install-file -DgroupId=' + group_id + ' -DartifactId='+ filename[:-4] + ' -Dversion=' + ver + ' -Dfile=' + jar_path + ' -Dpackaging=jar -DgeneratePom=true -DlocalRepositoryPath=./repository  -DcreateChecksum=true'
f_commands.write(command + '\n')

f.close()
f_commands.close()
